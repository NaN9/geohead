/*
  ==============================================================================

    SynthVoice.h
    Created: 6 Jul 2020 6:33:34pm
    Author:  eye

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "SynthSound.h"

class SynthVoice : public SynthesiserVoice
{
public:

    bool isVoiceActive() const override
    {
      return adsr.isActive();
    }

    bool canPlaySound (SynthesiserSound* sound) override
    {
        return dynamic_cast <SynthSound*>(sound) != nullptr;
    }

    void setPitchBend(int pitchWheelPos)
    {
        if (pitchWheelPos > 8192)
        {
            // shifting up
            pitchBend = float(pitchWheelPos - 8192) / (16383 - 8192);
        }
        else
        {
            // shifting down
            pitchBend = float(8192 - pitchWheelPos) / -8192;    // negative number
        }
    }

    float pitchBendCents()
    {
        if (pitchBend >= 0.0f)
        {
            // shifting up
            return pitchBend * pitchBendUpSemitones * 100;
        }
        else
        {
            // shifting down
            return pitchBend * pitchBendDownSemitones * 100;
        }
    }

    static double noteHz(int midiNoteNumber, double centsOffset)
    {
        double hertz = MidiMessage::getMidiNoteInHertz(midiNoteNumber);
        hertz *= std::pow(2.0, centsOffset / 1200);
        return hertz;
    }

    //=======================================================

    void startNote (int midiNoteNumber, float velocity, SynthesiserSound* sound, int currentPitchWheelPosition) override
    {
        adsr.noteOn();
        noteNumber = midiNoteNumber;
        setPitchBend(currentPitchWheelPosition);
        frequency = noteHz(noteNumber, pitchBendCents());
        level = velocity;
        phase_0 = 0.0f;

        spec.maximumBlockSize = samplesPerBlock_val;
        spec.numChannels = 2;
        spec.sampleRate = samplerate;

        filter.reset();
        filter.prepare (spec);

        spec_set = true;
    }

    //=======================================================

    void stopNote (float velocity, bool allowTailOff) override
    {
      adsr.noteOff();
      allowTailOff = true;

      if (velocity == 0)
        clearCurrentNote();

    }

    //=======================================================

    void pitchWheelMoved (int newPitchWheelValue) override
    {
        setPitchBend(newPitchWheelValue);
        frequency = noteHz(noteNumber, pitchBendCents());
    }

    //=======================================================

    void controllerMoved (int controllerNumber, int newControllerValue) override
    {

    }

    void setSampleRate(double lastSampleRate, int samplesPerBlock)
    {
      samplerate = lastSampleRate;
      samplesPerBlock_val = samplesPerBlock;

      adsr.setSampleRate(samplerate);
    }

    void setWavetable(std::array<std::array<float, 1081>, 2>* wavetable_h, int id_h)
    {
      wavetable = wavetable_h;
      id = id_h;
    }

    void setADSRParameters (std::atomic<float>* attack, std::atomic<float>* decay, std::atomic<float>* sustain, std::atomic<float>* release)
    {
      adsr_parameters.attack = *attack;
      adsr_parameters.decay = *decay;
      adsr_parameters.sustain = *sustain;
      adsr_parameters.release = *release;
    }

    void setGain(std::atomic<float>* gain_value)
    {
      gain = *gain_value;
    }

    void setTableRange(std::atomic<float>* start_sample_h, std::atomic<float>* stop_sample_h)
    {

        int start_max = (unsigned int)*stop_sample_h;
        int stop_min = (unsigned int) *start_sample_h + 8;

        if(*start_sample_h >= start_max)
        { first_sample = start_max; }
        else {first_sample = (unsigned int) *start_sample_h;}

        if(*stop_sample_h <= stop_min)
        { last_sample = stop_min; }
        else {last_sample = (unsigned int) *stop_sample_h;}

        wt_size = last_sample - first_sample;

    }

    void setTuning(std::atomic<float>* tuning)
    {
        tune = *tuning;
        if (tune > -1 && tune < 1 )
        {
          tune_holder = fabs(tune)+ 1;
        } else
        {
          tune_holder = fabs(tune) * 2;
        }
    }


    void renderNextBlock (AudioBuffer <float> &outputBuffer, int startSample, int numSamples) override
    {
      if(tune < 0){increment = (frequency / tune_holder) * wt_size / samplerate;}
      else{        increment = (frequency * tune_holder) * wt_size / samplerate;}

      adsr.setParameters(adsr_parameters);


        for (int sample = startSample; sample < (startSample + numSamples); ++sample)
        {
            env_value = adsr.getNextSample();

            int index_0 = (unsigned int) phase_0;
            int index_1 = index_0 == (wt_size - 1 ) ? (unsigned int) 0 : index_0 + 1;

            float frac = phase_0 - (float) index_0;

            float value_0 = (*wavetable)[id][first_sample + index_0];
            float value_1 = (*wavetable)[id][first_sample + index_1];

            float current_sample  = value_0 + frac * (value_1 - value_0);

            float sample_out = current_sample * level * env_value * gain;

            if(spec_set == true)
              filtered = filter.processSample(0, sample_out);

            for (int channel = 0; channel < outputBuffer.getNumChannels(); ++channel)
            {
              outputBuffer.addSample(channel, sample, filtered);
            }

            phase_0 = fmod(phase_0 + increment, wt_size);
        }
    }

    void updateFilter(std::atomic<float>* filt, std::atomic<float>* cut,
                      std::atomic<float>* res)
    {
      if (spec.sampleRate != 0.0)
      {
        filter.setCutoffFrequency (static_cast<float> (*cut));
        filter.setResonance       (static_cast<float> (*res));

        int switch_type = static_cast<int> (*filt);
        switch (switch_type)
        {
            case 0:
              filter.setType (dsp::StateVariableTPTFilterType::lowpass);
              break;
            case 1:
              filter.setType (dsp::StateVariableTPTFilterType::highpass);
              break;
            case 2:
              filter.setType (dsp::StateVariableTPTFilterType::bandpass);
              break;
            default:
             jassertfalse;
             break;
        };
      }
    }

private:
  float level = 0.0f;
  double frequency = 0.0;
  int noteNumber;

  float pitchBend = 0.0f;
  float pitchBendUpSemitones = 2.0f;
  float pitchBendDownSemitones = 2.0f;
  float gain;

  unsigned int wt_size = 1080;
  int first_sample = 0;
  int last_sample = 0;

  float phase_0 = 0.0f;
  float increment = 0.0f;
  double samplerate = 44100;
  std::array<std::array<float, 1081>, 2> instantiator = {{ {0}, {0} }};
  std::array<std::array<float, 1081>, 2>* wavetable = &instantiator;
  int id;

  ADSR adsr;
  ADSR::Parameters adsr_parameters;

  float tune = 0.0f;
  float tune_holder = 0.0f;

  dsp::StateVariableTPTFilter<float> filter;
  dsp::ProcessSpec spec;

  int samplesPerBlock_val = 0;
  bool spec_set = false;

  float filtered = 0.0f;
  float env_value = 0.0f;

};
