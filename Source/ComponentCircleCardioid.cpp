/*
  ==============================================================================

    ComponentCircleCardioid.cpp
    Created: 21 Jul 2020 6:35:29pm
    Author:  eye

  ==============================================================================
*/

#include <JuceHeader.h>
#include "ComponentCircleCardioid.h"
#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
ComponentCircleCardioid::ComponentCircleCardioid(GeoHeadAudioProcessor& p, GeoHeadAudioProcessorEditor& e, int componentId)
: audioProcessor(p), audioEditor(e), id(componentId)
{
  pointSlider.setSliderStyle (Slider::SliderStyle::Rotary);
  pointSlider.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
  pointSlider.setRange (0.0f, 555.0f, 1.0f);
  pointSlider.setValue (10);
  pointSlider.addListener (this);
  pointSlider.setAlwaysOnTop(true);
  pointSlider.setRotaryParameters(-3.1415f, 15.7f, true);
  pointSlider.setLookAndFeel(&knobPurple);
  pointAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, parameter_ids[id][0], pointSlider);

  factorSlider.setSliderStyle (Slider::SliderStyle::Rotary);
  factorSlider.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
  factorSlider.setRange (0.0f, 23.0f, 1.0f);
  factorSlider.setValue (2);
  factorSlider.addListener (this);
  factorSlider.setLookAndFeel(&knobPurple);
  factorAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters,  parameter_ids[id][1], factorSlider);

  courseSlider.setSliderStyle (Slider::SliderStyle::Rotary);
  courseSlider.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
  courseSlider.setRange (0.000f, 1.0f, 0.003f);
  courseSlider.setValue (1.00f);
  courseSlider.addListener (this);
  courseSlider.setLookAndFeel(&knobPurple);
  courseAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters,  parameter_ids[id][2], courseSlider);

  measureSlider.setSliderStyle (Slider::SliderStyle::Rotary);
  measureSlider.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
  measureSlider.setRange (0.000f, 127.0f, 1.0f);
  measureSlider.setValue (127.00f);
  measureSlider.addListener (this);
  measureSlider.setAlwaysOnTop(true);
  measureSlider.setLookAndFeel(&knobPurple);
  measureAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters,  parameter_ids[id][3], measureSlider);

  addAndMakeVisible (pointSlider);
  addAndMakeVisible (factorSlider);
  addAndMakeVisible (courseSlider);
  addAndMakeVisible (measureSlider);

}

ComponentCircleCardioid::~ComponentCircleCardioid()
{
}

void ComponentCircleCardioid::paint (Graphics& g)
{

    Image background = ImageCache::getFromMemory (BinaryData::background_circle_png, BinaryData::background_circle_pngSize);
    g.drawImageAt (background, 30, 8);

    g.setColour (juce::Colours::black);
    g.drawEllipse(34, 10, 488, 488, 3); //488
    g.setColour(juce::Colours::grey);


    for(int i = 0; i < points; i++)
    {
      //Calculate points on Circle
      t = MathConstants<float>::twoPi * i / points;
      x = static_cast<int> (round (34 + radius + radius * cos(t)));
      y = static_cast<int> (round (10 + radius + radius * sin(t)));

      if (points < 99){
        g.drawEllipse(x - 1, y - 1, 2, 2, 1);
      }

      t_2 = MathConstants<float>::twoPi * (i * (factor + course)) / points;
      x_2 = static_cast<int> (round (34 + radius + radius * cos(t_2) ));
      y_2 = static_cast<int>(round (10 + radius + radius * sin(t_2) ));

      g.drawLine(x, y, x_2, y_2, 2.0f);
    }
}

void ComponentCircleCardioid::resized()
{
  pointSlider.setBounds(6, 21, 86, 86);
  factorSlider.setBounds(getWidth() - 80, 22, 86, 86);
  courseSlider.setBounds(6, getHeight() - 82, 86, 86);
  measureSlider.setBounds(getWidth() - 80, getHeight() - 82, 86, 86);

}

void ComponentCircleCardioid::sliderValueChanged(Slider *slider)
{
  if(slider == &pointSlider)
  {
    points = static_cast<int>(pointSlider.getValue());
  }
  if(slider == &factorSlider)
  {
    factor = static_cast<int>(factorSlider.getValue());
  }

  if(slider == &courseSlider)
  {
    course = courseSlider.getValue();
  }
  repaint();
  audioEditor.repaint();
}
