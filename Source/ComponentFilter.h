/*
  ==============================================================================

    ComponentFilter.h
    Created: 24 Jul 2020 10:15:55am
    Author:  eye

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "myLookAndFeel.h"

//==============================================================================
/*
*/
class ComponentFilter  : public juce::Component
{
public:
    ComponentFilter(GeoHeadAudioProcessor&, int componentId);
    ~ComponentFilter() override;

    void paint (juce::Graphics&) override;
    void resized() override;

private:
    GeoHeadAudioProcessor& audioProcessor;
    int id;

    myLookAndFeelV1 knobR {Colour(246, 140, 30), "R", false, true};
    myLookAndFeelV1 knobC {Colour(246, 140, 30), "C", false, true};

    //==========================================================================
    const String title_ids[2] = { "FILTER 1", "FILTER 2"};
    ComboBox filterMenu;
    Slider filterCutoff;
    Slider filterRes;


    //==========================================================================
    const String parameter_ids[2][3] = { {"FILTER_TYPE_ID", "CUTOFF_ID", "RESONANCE_ID"}, {"FILTER_TYPE_ID_1", "CUTOFF_ID_1", "RESONANCE_ID_1"} };

    std::unique_ptr <AudioProcessorValueTreeState::ComboBoxAttachment> filterTypeAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> filterCutoffAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> filterResonanceAttachment;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ComponentFilter)
};
