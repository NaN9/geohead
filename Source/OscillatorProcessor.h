/*
  ==============================================================================

    oscillator.h
    Created: 31 Jul 2020 10:21:05am
    Author:  eye

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "ProcessorBase.h"
#include "SynthSound.h"
#include "SynthVoice.h"

class OscillatorProcessor  : public ProcessorBase
{
public:

  OscillatorProcessor(int osc_id)
  {
    id = osc_id;

    synth.clearVoices();

    for (int i = 0; i < 32; i++)
    {
        synth.addVoice(new SynthVoice());
    }

    synth.clearSounds();
    synth.addSound(new SynthSound());
  }

  void prepareToPlay (double sampleRate, int samplesPerBlock) override
  {
    lastSampleRate = sampleRate;
    lastSamplesPerBlock = samplesPerBlock;
    synth.setCurrentPlaybackSampleRate(lastSampleRate);

  }

  void processBlock (juce::AudioSampleBuffer& buffer, juce::MidiBuffer& midiMessages) override
  {
    for (int i = 0; i < synth.getNumVoices(); i++)
    {
        //if myVoice sucessfully casts as a SynthVoice*, get the voice and set the params
        if ((voice = dynamic_cast<SynthVoice*>(synth.getVoice(i))))
        {
          voice->setSampleRate(lastSampleRate, lastSamplesPerBlock);
          voice->updateFilter(filt, cut, res);
          voice->setWavetable(wavetable, id);
          voice->setADSRParameters(att, dec, sus, rel);
          voice->setGain(gain);
          voice->setTableRange(first_sample, last_sample);
          voice->setTuning(tune);
        }
    }

    synth.renderNextBlock(buffer, midiMessages, 0, buffer.getNumSamples());
  }

  void setWavetable(std::array<std::array<float, 1081>, 2>* wavetable_h)
  {
    wavetable = wavetable_h;
  }

  void setParameters (std::atomic<float>* attack, std::atomic<float>* decay,
                      std::atomic<float>* sustain, std::atomic<float>* release,
                      std::atomic<float>* gain_value, std::atomic<float>* tuning,
                      std::atomic<float>* start_sample, std::atomic<float>* stop_sample,
                      std::atomic<float>* filter_type, std::atomic<float>* cutoff,
                      std::atomic<float>* resonance)
  {
    att = attack;
    dec = decay;
    sus = sustain;
    rel = release;

    gain = gain_value;
    tune = tuning;

    first_sample = start_sample;
    last_sample = stop_sample;

    filt = filter_type;
    cut = cutoff;
    res = resonance;

  }

  void reset() override
  {
    filter.reset();
  }


private:
    double lastSampleRate;
    Synthesiser synth;
    SynthVoice* voice, voice_1, voice_2, voice_3, voice_4;

    int id;

    int lastSamplesPerBlock;

    std::atomic<float>* att = nullptr;
    std::atomic<float>* dec = nullptr;
    std::atomic<float>*sus = nullptr;
    std::atomic<float>* rel = nullptr;
    std::atomic<float>* gain = nullptr;
    std::atomic<float>* first_sample = nullptr;
    std::atomic<float>* last_sample = nullptr;
    std::atomic<float>* tune = nullptr;
    std::atomic<float>* filt = nullptr;
    std::atomic<float>* cut = nullptr;
    std::atomic<float>* res = nullptr;

    std::array<std::array<float, 1081>, 2> instantiator = {{ {0}, {0} }};
    std::array<std::array<float, 1081>, 2>* wavetable = &instantiator;

    //==============================================================================
    dsp::StateVariableTPTFilter<float> filter;
};
