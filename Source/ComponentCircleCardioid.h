/*
  ==============================================================================

    ComponentCircleCardioid.h
    Created: 21 Jul 2020 6:35:29pm
    Author:  eye

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "myLookAndFeel.h"
class GeoHeadAudioProcessorEditor;

//==============================================================================
/*
*/
class ComponentCircleCardioid  : public Component,
                                 public Slider::Listener
{
public:
    ComponentCircleCardioid(GeoHeadAudioProcessor&, GeoHeadAudioProcessorEditor&, int componentId);
    ~ComponentCircleCardioid() override;

    void paint (Graphics&) override;
    void resized() override;

    void sliderValueChanged (Slider *slider);

private:
    GeoHeadAudioProcessor& audioProcessor;
    GeoHeadAudioProcessorEditor& audioEditor;
    int id;

    myLookAndFeelV1 knobPurple {Colour(112, 83, 163), " ", false, true};

    //==========================================================================
    int radius = 244;
    float t, t_2;
    int x, y, x_2, y_2;

    int points = 10, factor = 2;
    float course = 0;

    //==========================================================================
    Slider pointSlider, factorSlider, courseSlider, measureSlider;

    //==========================================================================
    const String parameter_ids[2][4] = { {"POINT_ID", "FACTOR_ID", "COURSE_ID", "MEASURE_ID" }, {"POINT_ID_1", "FACTOR_ID_1", "COURSE_ID_1", "MEASURE_ID_1"}};

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ComponentCircleCardioid)

public:
  std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> pointAttachment;
  std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> factorAttachment;
  std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> courseAttachment;
  std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> measureAttachment;
};
