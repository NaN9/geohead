/*
  ==============================================================================

    ComponentWavetable.cpp
    Created: 21 Jul 2020 8:44:52pm
    Author:  eye

  ==============================================================================
*/

#include <JuceHeader.h>
#include "ComponentWavetable.h"


//==============================================================================
ComponentWavetable::ComponentWavetable(GeoHeadAudioProcessor& p, int componentId)
: audioProcessor (p), id (componentId)
{
  addAndMakeVisible (modeSelektor);
  modeSelektor.addSectionHeading ("Cardioid");
  PopupMenu subMenuTravelerCard;
  modeSelektor.addItem("Sine", 1);
  modeSelektor.addItem("Square", 2);
  modeSelektor.addItem("Saw", 3);
  modeSelektor.addItem ("Triangle", 4);

  modeSelektor.addSectionHeading("Asteroid");
  modeSelektor.addItem ("Coming Soon", 5);
  modeSelektor.addItem ("Coming Soon", 6);
  modeSelektor.setColour(ComboBox::backgroundColourId, Colour(5, 7, 8));


  modeSelektor.onChange = [this] { modeSelektorChanged(); };
  modeSelektor.setSelectedId (1);

  modeAttachment = std::make_unique<AudioProcessorValueTreeState::ComboBoxAttachment>(audioProcessor.parameters, parameter_ids[id][0], modeSelektor);
  //==========================================================================
  button_left.addListener(this);
  button_right.addListener(this);
  button_left.setClickingTogglesState (true);
  button_right.setClickingTogglesState (true);
  button_left.setLookAndFeel(&arrowLeft);
  button_right.setLookAndFeel(&arrowRight);

  addAndMakeVisible(button_left);
  addAndMakeVisible(button_right);

  buttonLAttachment = std::make_unique<AudioProcessorValueTreeState::ButtonAttachment>(audioProcessor.parameters, parameter_ids[id][1], button_left);
  buttonRAttachment = std::make_unique<AudioProcessorValueTreeState::ButtonAttachment>(audioProcessor.parameters, parameter_ids[id][2], button_right);

  //============================================================================
  attackSlider.setSliderStyle (Slider::SliderStyle::Rotary);
  attackSlider.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
  attackSlider.setRange (0.01f, 5.0f);
  attackSlider.setValue (0.01f);
  //attackSlider.addListener (this);
  attackSlider.setAlwaysOnTop(true);
  attackSlider.setLookAndFeel(&knobA);
  attackAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, parameter_ids[id][3], attackSlider);

  decaySlider.setSliderStyle (Slider::SliderStyle::Rotary);
  decaySlider.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
  decaySlider.setRange (0.01f, 2.0f);
  decaySlider.setValue (0.01f);
  //decaySlider.addListener (this);
  decaySlider.setAlwaysOnTop(true);
  decaySlider.setLookAndFeel(&knobD);
  decayAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, parameter_ids[id][4], decaySlider);

  sustainSlider.setSliderStyle (Slider::SliderStyle::Rotary);
  sustainSlider.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
  sustainSlider.setRange (0.0f, 1.0f);
  sustainSlider.setValue (1.0f);
  //sustainSlider.addListener (this);
  sustainSlider.setAlwaysOnTop(true);
  sustainSlider.setLookAndFeel(&knobS);
  sustainAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, parameter_ids[id][5], sustainSlider);

  releaseSlider.setSliderStyle (Slider::SliderStyle::Rotary);
  releaseSlider.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
  releaseSlider.setRange (0.01f, 5.0f);
  releaseSlider.setValue (0.01f);
  //releaseSlider.addListener (this);
  releaseSlider.setAlwaysOnTop(true);
  releaseSlider.setLookAndFeel(&knobR);
  releaseAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, parameter_ids[id][6], releaseSlider);

  addAndMakeVisible (attackSlider);
  addAndMakeVisible (decaySlider);
  addAndMakeVisible (sustainSlider);
  addAndMakeVisible (releaseSlider);

  //============================================================================

  leftBoundarySlider.setSliderStyle (Slider::SliderStyle::Rotary);
  leftBoundarySlider.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
  leftBoundarySlider.setRange (0.0f, 1072.0f);
  leftBoundarySlider.setValue (0.0f);
  leftBoundarySlider.addListener (this);
  leftBoundarySlider.setAlwaysOnTop(true);
  leftBoundarySlider.setLookAndFeel(&knobCyan);
  leftBoundarySlider.setRotaryParameters(-MathConstants<float>::pi, MathConstants<float>::pi, true);
  leftBoundaryAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, parameter_ids[id][7], leftBoundarySlider);

  rightBoundarySlider.setSliderStyle (Slider::SliderStyle::Rotary);
  rightBoundarySlider.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
  rightBoundarySlider.setRange (8.0f, 1080.0f);
  rightBoundarySlider.setValue (1080.0f);
  rightBoundarySlider.addListener (this);
  rightBoundarySlider.setAlwaysOnTop(true);
  rightBoundarySlider.setLookAndFeel(&knobCyan);
  right_min = MathConstants<float>::twoPi * ( 8.0f / 1080.0f) - MathConstants<float>::pi;
  rightBoundarySlider.setRotaryParameters(-MathConstants<float>::pi, MathConstants<float>::pi, true);
  rightBoundaryAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, parameter_ids[id][8], rightBoundarySlider);

  addAndMakeVisible(leftBoundarySlider);
  addAndMakeVisible(rightBoundarySlider);

  //============================================================================
  addAndMakeVisible(phaseButton);
  phaseButton.addListener(this);
  phaseButton.setClickingTogglesState(true);
  phaseButton.setLookAndFeel(&phaseImg);
  phaseButtonAttachment = std::make_unique<AudioProcessorValueTreeState::ButtonAttachment>(audioProcessor.parameters, parameter_ids[id][9], phaseButton);


}

ComponentWavetable::~ComponentWavetable()
{
}

void ComponentWavetable::paint (juce::Graphics& g)
{
    g.setColour (Colours::white);
    g.setFont (15.0f);

    switch(algorithm)
    {
      case 1: g.drawText 	( "DUTTY 1", 229, 9, 100, 20, Justification::centred);
              break;
      case 2: g.drawText 	( "DUTTY 0", 229, 9, 100, 20, Justification::centred);
              break;
      case 3: g.drawText 	( "STEPPER", 229, 9, 100, 20, Justification::centred);
              break;
    }
    int top_box = getHeight() - 99 - 172;
    y_middle = 86 + top_box;

    ColourGradient gr_box = ColourGradient(Colour(0,0,0), 0, 0, Colour(39,38,38), 528, 0, false);
    g.setGradientFill(gr_box);
    g.fillRoundedRectangle (15, top_box, 534, 172, 5);

    ColourGradient gr = ColourGradient(Colour(252,181,49), 0, 0, Colour(69,181,232), 528, 0, false);
    gr.addColour(0.25, Colour(236,62,85));
    gr.addColour(0.74, Colour(123,73,157));


    Path waveform_path;
    x_path_waveform = 18;
    y_path_waveform = y_middle;

    waveform_path.startNewSubPath (x_path_waveform, y_path_waveform);

    for (int i = 0; i <= 1080; i++)
    {
      y_path_waveform = static_cast<int>(round (y_middle - audioProcessor.waveforms[id][i] * 81));
      x_path_waveform = 18 + static_cast<int>(round (i * 528 / 1080));

      waveform_path.lineTo(x_path_waveform, y_path_waveform);
    }
    g.setGradientFill(gr);
    g.strokePath (waveform_path, PathStrokeType (2.1f));

    //Fill range inverse
    g.setColour(Colours::black);
    g.setOpacity(0.8f);
    g.fillRoundedRectangle(15, top_box, left_boundary, 172, 5);
    g.fillRoundedRectangle(15 + right_boundary, top_box, 534, 172, 5);

}

void ComponentWavetable::modeSelektorChanged()
{
  mode = modeSelektor.getSelectedId();
  repaint();
}

void ComponentWavetable::buttonClicked(Button *button)
{
  if(button == &button_left)
  {
    if(algorithm < 2)
    {
      algorithm = 3;
    } else
    {
      algorithm--;
    }
  }
  if(button == &button_right)
  {
    if(algorithm > 2)
    {
      algorithm = 1;
    } else
    {
      algorithm++;
    }
  }

  repaint();
}

void ComponentWavetable::sliderValueChanged (Slider *slider)
{
  if(slider == &leftBoundarySlider)
  {
    left_boundary_f = leftBoundarySlider.getValue();
    right_min = MathConstants<float>::twoPi * ( (left_boundary_f + 8.0f ) / 1080.0f) - MathConstants<float>::pi;
    rightBoundarySlider.setRotaryParameters(right_min, MathConstants<float>::pi + 0.13, true);
    rightBoundarySlider.setRange(left_boundary_f + 8.0f, 1080.0f);

    left_boundary = static_cast<int>(round (left_boundary_f * 528 / 1080));
    //std::cout << id << ": " << *audioProcessor.parameters.getRawParameterValue(parameter_ids[id][7]) << std::endl;
  }

  if(slider == &rightBoundarySlider)
  {
    right_boundary_f = rightBoundarySlider.getValue();
    left_max = MathConstants<float>::twoPi * ((right_boundary_f - 8.0f) / 1080.0f) - MathConstants<float>::pi;
    leftBoundarySlider.setRotaryParameters(-MathConstants<float>::pi + 0.13 , left_max, true);
    leftBoundarySlider.setRange(0.0f, right_boundary_f - 8.0f);

    right_boundary = static_cast<int>(round (right_boundary_f * 528 / 1080));
    right_boundary += 3;
    //std::cout << id << ": " << *audioProcessor.parameters.getRawParameterValue(parameter_ids[id][8]) << std::endl;
  }

  repaint();
}

void ComponentWavetable::resized()
{
  modeSelektor.setBounds(23, 5, 95, 20);
  button_left.setBounds(204, 5 , 27, 27);
  button_right.setBounds(321, 5 , 27, 27);

  attackSlider.setBounds(13, getHeight() - 96, 86, 86);
  decaySlider.setBounds(90, getHeight() - 96, 86, 86);
  sustainSlider.setBounds(167, getHeight() - 96, 86, 86);
  releaseSlider.setBounds(244, getHeight() - 96, 86, 86);

  phaseButton.setBounds(326, getHeight() - 77, 50, 50);

  leftBoundarySlider.setBounds(371, getHeight() - 96, 86, 86);
  rightBoundarySlider.setBounds(448, getHeight() - 96, 86, 86);
}
