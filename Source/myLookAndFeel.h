/*
  ==============================================================================

    myLookAndFeel.h
    Created: 21 Oct 2020 1:09:54pm
    Author:  eye

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>

class myLookAndFeelV1 : public LookAndFeel_V4
{
public:
    myLookAndFeelV1(Colour c, String s, bool f, bool sm);

    void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos,
        float rotaryStartAngle, float rotaryEndAngle, Slider& slider) override;

private:
    Image img1;
    Colour col;
    String letter;
    bool fullCircle;
    bool small;

    Typeface::Ptr tface = nullptr;

    int size = 0;
    int rxy = 0;
    int radius = 0;
};

class myLookAndFeelV2 : public LookAndFeel_V4
{
public:
    myLookAndFeelV2(bool l);

    void drawButtonBackground (juce::Graphics& g, juce::Button& button, const juce::Colour& backgroundColour,
                               bool, bool isButtonDown) override;

private:
    Image arrow_img;
    bool left;
};

class myLookAndFeelV3 : public LookAndFeel_V4
{
public:
    myLookAndFeelV3();

    void drawButtonBackground (juce::Graphics& g, juce::Button& button, const juce::Colour& backgroundColour,
                               bool, bool isButtonDown) override;

private:
    Image phase_up;
    Image phase_down;
};
