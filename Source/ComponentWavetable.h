/*
  ==============================================================================

    ComponentWavetable.h
    Created: 21 Jul 2020 8:44:52pm
    Author:  eye

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "myLookAndFeel.h"

//==============================================================================
/*
*/
class ComponentWavetable  : public juce::Component,
                            public Button::Listener,
                            public Slider::Listener
{
public:
    ComponentWavetable(GeoHeadAudioProcessor&, int componentId);
    ~ComponentWavetable() override;

    void paint (juce::Graphics&) override;
    void resized() override;

    void modeSelektorChanged();
    void buttonClicked (Button *button);
    void sliderValueChanged (Slider *slider);

private:
    GeoHeadAudioProcessor& audioProcessor;
    int id;

    myLookAndFeelV1 knobA {Colour(56, 84, 165), "A", false, true};
    myLookAndFeelV1 knobD {Colour(56, 84, 165), "D", false, true};
    myLookAndFeelV1 knobS {Colour(56, 84, 165), "S", false, true};
    myLookAndFeelV1 knobR {Colour(56, 84, 165), "R", false, true};

    myLookAndFeelV1 knobCyan {Colour(2, 179, 220),  " ", true, true};
    myLookAndFeelV2 arrowLeft  {true};
    myLookAndFeelV2 arrowRight {false};

    myLookAndFeelV3 phaseImg;

    int y_middle;
    int x_path_waveform;
    int y_path_waveform;

    //==========================================================================
    ComboBox modeSelektor;
    int mode;

    TextButton button_left {" "};
    TextButton button_right {" "};

    int algorithm = 1;

    Slider attackSlider, decaySlider, sustainSlider, releaseSlider;

    TextButton phaseButton {" "};

    Slider leftBoundarySlider, rightBoundarySlider;
    int left_boundary = 0, right_boundary = 528;
    float right_boundary_f, left_boundary_f;
    float left_max, right_min;



    const String parameter_ids[2][10] = { {"MODE_ID", "BUTTON_L_ID", "BUTTON_R_ID",
                                          "ATTACK_ID", "DECAY_ID", "SUSTAIN_ID",
                                          "RELEASE_ID", "LEFT_BOUND_ID",
                                          "RIGHT_BOUND_ID", "PHASE"},
                                         {"MODE_ID_1", "BUTTON_L_ID_1", "BUTTON_R_ID_1",
                                          "ATTACK_ID_1", "DECAY_ID_1", "SUSTAIN_ID_1",
                                          "RELEASE_ID_1", "LEFT_BOUND_ID_1",
                                          "RIGHT_BOUND_ID_1", "PHASE_1"} };

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ComponentWavetable)

public:
    std::unique_ptr <AudioProcessorValueTreeState::ComboBoxAttachment> modeAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::ButtonAttachment> buttonLAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::ButtonAttachment> buttonRAttachment;

    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> attackAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> decayAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> sustainAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> releaseAttachment;

    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> leftBoundaryAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> rightBoundaryAttachment;

    std::unique_ptr <AudioProcessorValueTreeState::ButtonAttachment> phaseButtonAttachment;

};
