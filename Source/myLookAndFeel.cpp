/*
  ==============================================================================

    myLookAndFeel.cpp
    Created: 21 Oct 2020 1:09:54pm
    Author:  eye

  ==============================================================================
*/

#include "myLookAndFeel.h"

myLookAndFeelV1::myLookAndFeelV1(Colour c, String s, bool f, bool sm) : col(c), letter(s), fullCircle(f), small(sm)
{
    if(small)
    {
      img1 = ImageCache::getFromMemory (BinaryData::knob1_png, BinaryData::knob1_pngSize);
    } else

    {
      img1 = ImageCache::getFromMemory (BinaryData::knob2_png, BinaryData::knob2_pngSize);
    }

    tface = Typeface::createSystemTypefaceFor(BinaryData::DINPro_Light_ttf, BinaryData::DINPro_Light_ttfSize);
    setDefaultSansSerifTypeface(tface);
}

//==============================================================================
void myLookAndFeelV1::drawRotarySlider(Graphics& g,
    int x, int y, int width, int height, float sliderPos,
    float rotaryStartAngle, float rotaryEndAngle, Slider& slider)
{

      float angle = rotaryStartAngle + (sliderPos * (rotaryEndAngle - rotaryStartAngle));
      if(small)
      {
        size = 51;
        rxy = 18;
        radius = 30;
      } else
      {
        size = 66;
        rxy = 15;
        radius = 38;
      }

      //Draw arc around knob
      Path rotation_grey;
      g.setColour(Colour(59, 59, 59));
      rotation_grey.addArc(rxy, rxy, size, size, rotaryStartAngle, rotaryEndAngle, true);
      g.strokePath(rotation_grey, PathStrokeType(3.0f));

      Path rotation_colour;
      rotation_colour.addArc(rxy, rxy, size, size, rotaryStartAngle, angle, true);
      g.setColour(col);
      g.strokePath(rotation_colour, PathStrokeType (3.0f) );

      g.drawImageAt(img1, 0, 0);

      //g.setColour(Colours::white);
      //Draw dots around knob

      int center = width/2;
      for(int i=0;i<12;i++)
      {
        float t = MathConstants<float>::twoPi * i / 12;
        x = static_cast<int> (round ( center + radius * cos(t)));
        y = static_cast<int> (round ( center + radius * sin(t)));

        if (fullCircle){
          g.drawEllipse(x , y , 0.7, 0.7, 0.6);
        } else{
          if(i == 2 || i == 3 || i == 4){} else
            {g.drawEllipse(x , y , 0.7, 0.7, 0.6);}
        }
      }
        //Draw letter in center
        g.setFont(tface);
        g.setFont (18.0f);
        g.setColour(Colour(186, 186, 187));
        g.drawText(letter, 0, 0, width, height, juce::Justification::centred, false);
}

myLookAndFeelV2::myLookAndFeelV2(bool l) : left(l)
{
    if(left)
    {
      arrow_img = ImageCache::getFromMemory (BinaryData::arrow_L_png, BinaryData::arrow_L_pngSize);
    } else
    {
      arrow_img = ImageCache::getFromMemory (BinaryData::arrow_png, BinaryData::arrow_pngSize);
    }
}

void myLookAndFeelV2::drawButtonBackground (juce::Graphics& g, juce::Button& button, const juce::Colour& backgroundColour,
                           bool, bool isButtonDown)
{
  g.drawImageAt(arrow_img, 0, 0);
}


myLookAndFeelV3::myLookAndFeelV3()
{

      phase_down = ImageCache::getFromMemory (BinaryData::phase_down_png, BinaryData::phase_down_pngSize);
      phase_up = ImageCache::getFromMemory (BinaryData::phase_up_png, BinaryData::phase_up_pngSize);
}

void myLookAndFeelV3::drawButtonBackground (juce::Graphics& g, juce::Button& button, const juce::Colour& backgroundColour,
                           bool, bool isButtonDown)
{
  if(isButtonDown)
  {
    g.drawImageAt(phase_down, 0, 0);
  } else
  {
    g.drawImageAt(phase_up, 0, 0);
  }


}
