/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "OscillatorProcessor.h"

//==============================================================================
/**
*/
class GeoHeadAudioProcessor  : public juce::AudioProcessor,
                               private AudioProcessorValueTreeState::Listener
{
public:
    //==============================================================================
    GeoHeadAudioProcessor();
    ~GeoHeadAudioProcessor() override;

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

    //==============================================================================
    void setParameters();
    void createWaveformTemplates();

    void createWavetableCardioid(std::atomic<float>* mode, std::atomic<float>* points,
                            std::atomic<float>* factor, std::atomic<float>* course,
                            std::atomic<float>* measure, int id);

    //==============================================================================


    //==============================================================================
    std::array<std::array<float, 1081>, 2> waveforms = {{ {0}, {0} }};
    float waveform_holders[2][1080] = { {0}, {0} };
    bool waveform_set = false;

    //==============================================================================
    AudioProcessorValueTreeState parameters;

private:

    void parameterChanged(const String &parameterID, float newValue);

    //==============================================================================
    std::atomic<float>* modeParameter = nullptr;
    std::atomic<float>* pointParameter = nullptr;
    std::atomic<float>* factorParameter = nullptr;
    std::atomic<float>* courseParameter = nullptr;
    std::atomic<float>* measureParameter = nullptr;

    std::atomic<float>* modeParameter_1 = nullptr;
    std::atomic<float>* pointParameter_1 = nullptr;
    std::atomic<float>* factorParameter_1 = nullptr;
    std::atomic<float>* courseParameter_1 = nullptr;
    std::atomic<float>* measureParameter_1 = nullptr;


    //==============================================================================
    std::atomic<float>* attackParameter = nullptr;
    std::atomic<float>* decayParameter = nullptr;
    std::atomic<float>* sustainParameter = nullptr;
    std::atomic<float>* releaseParameter = nullptr;

    std::atomic<float>* attackParameter_1 = nullptr;
    std::atomic<float>* decayParameter_1 = nullptr;
    std::atomic<float>* sustainParameter_1 = nullptr;
    std::atomic<float>* releaseParameter_1 = nullptr;

    //==============================================================================
    std::atomic<float>* buttonAlgoLeftParameter = nullptr;
    std::atomic<float>* buttonAlgoRightParameter = nullptr;

    std::atomic<float>* buttonAlgoLeftParameter_1 = nullptr;
    std::atomic<float>* buttonAlgoRightParameter_1 = nullptr;

    //==============================================================================
    std::atomic<float>* gainParameter = nullptr;
    std::atomic<float>* gainParameter_1 = nullptr;

    //==============================================================================
    int vec_pos; // postCardioidWaveform

    //==============================================================================
    float t, t_2, x, y; // createWavetableCardioid
    float radian_x, radian_y, radian_x_2, radian_y_2;
    float rad_wav, pos_wav;
    float chord_lenght;

    //==============================================================================
    float waveform_templates[4][1080] = { {0}, {0}, {0}, {0}};

    //==============================================================================
    int algorithm[2] = { 1, 1};
    float phase[2] = { 1.0f, 1.0f};

    //==============================================================================
    double lastSampleRate;

    //==========================================================================
    OscillatorProcessor proc0 {0};
    OscillatorProcessor proc1 {1};

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GeoHeadAudioProcessor)
};
