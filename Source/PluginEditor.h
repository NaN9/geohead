#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "ComponentCircleCardioid.h"
#include "ComponentWavetable.h"
#include  "ComponentFilter.h"
#include "myLookAndFeel.h"

//==============================================================================
/**
*/
class GeoHeadAudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    GeoHeadAudioProcessorEditor (GeoHeadAudioProcessor&);
    ~GeoHeadAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    GeoHeadAudioProcessor& audioProcessor;

    //================================================================================
    myLookAndFeelV1 knobOrange {Colour(254, 188, 17), "T", false, false};
    myLookAndFeelV1 knobRed {Colour(255, 31, 66), "G", false, false};

    Image logo = ImageCache::getFromMemory (BinaryData::logo_png, BinaryData::logo_pngSize);

    ComponentCircleCardioid cardioidComponent {audioProcessor, *this, 0};
    ComponentCircleCardioid cardioidComponent_1 {audioProcessor, *this, 1};

    ComponentWavetable wavetableComponent {audioProcessor, 0};
    ComponentWavetable wavetableComponent_1 {audioProcessor, 1};

    ComponentFilter filterComponent {audioProcessor, 0};
    ComponentFilter filterComponent_1 {audioProcessor, 1};

    Slider gainSlider, gainSlider_1, tuneSlider, tuneSlider_1;

    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> gainAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> gainAttachment_1;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> tuneAttachment;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> tuneAttachment_1;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GeoHeadAudioProcessorEditor)
};
