/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
GeoHeadAudioProcessor::GeoHeadAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                   ),
       parameters (*this, nullptr, Identifier ("PARAMETERS"),
                     {
                        std::make_unique<AudioParameterInt> ("MODE_ID", "Mode", 1, 6, 1),
                        std::make_unique<AudioParameterInt>("POINT_ID", "Points", 0, 555, 0),
                        std::make_unique<AudioParameterInt>("FACTOR_ID", "Factor", 0, 23, 2),
                        std::make_unique<AudioParameterFloat>("COURSE_ID", "Course", NormalisableRange<float> (0.000f, 1.0f), 1.0f),
                        std::make_unique<AudioParameterInt>("MEASURE_ID", "Measure", 0, 144, 127),
                        std::make_unique<AudioParameterFloat>("ATTACK_ID", "Attack", NormalisableRange<float> (0.01f, 5.0f), 0.01),
                        std::make_unique<AudioParameterFloat>("DECAY_ID", "Decay", NormalisableRange<float> (0.01f, 2.0f), 0.01),
                        std::make_unique<AudioParameterFloat>("SUSTAIN_ID", "Sustain", NormalisableRange<float> (0.0f, 1.0f), 1),
                        std::make_unique<AudioParameterFloat>("RELEASE_ID", "Release", NormalisableRange<float> (0.01f, 5.0f), 0),
                        std::make_unique<AudioParameterBool> ("BUTTON_L_ID", "Left button", false),
                        std::make_unique<AudioParameterBool> ("BUTTON_R_ID", "Right button", false),
                        std::make_unique<AudioParameterFloat>("FILTER_TYPE_ID", "Filter type", NormalisableRange<float>(0.0f, 2.0f), 0.0f),
                        std::make_unique<AudioParameterFloat>("CUTOFF_ID", "Filter cutoff", NormalisableRange<float>(20.0f, 16000.0f), 8000.0f),
                        std::make_unique<AudioParameterFloat>("RESONANCE_ID", "Filter resonance", NormalisableRange<float>(1.0f, 5.0f), 1.0f),
                        std::make_unique<AudioParameterFloat>("LEFT_BOUND_ID", "Start sample", NormalisableRange<float>(0.0f, 1080.0f), 0.0f),
                        std::make_unique<AudioParameterFloat>("RIGHT_BOUND_ID", "End sample", NormalisableRange<float>(0.0f, 1080.0f), 1080.0f),
                        std::make_unique<AudioParameterFloat>("GAIN_ID", "Gain Osc 1", NormalisableRange<float>(0.0f, 1.0f), 0.6f),
                        std::make_unique<AudioParameterFloat>("TUNE_ID", "Tune Osc 1", NormalisableRange<float>(-2.0f, 2.0f), 0.0f),
                        std::make_unique<AudioParameterBool> ("PHASE", "Invert phase", false),

                        std::make_unique<AudioParameterInt> ("MODE_ID_1", "Mode 2", 1, 6, 1),
                        std::make_unique<AudioParameterInt>("POINT_ID_1", "Points 2", 0, 555, 10),
                        std::make_unique<AudioParameterInt>("FACTOR_ID_1", "Factor 2", 0, 23, 2),
                        std::make_unique<AudioParameterFloat>("COURSE_ID_1", "Course 2", NormalisableRange<float> (0.000f, 1.0f), 0),
                        std::make_unique<AudioParameterInt>("MEASURE_ID_1", "Measure 2", 0, 144, 127),
                        std::make_unique<AudioParameterFloat>("ATTACK_ID_1", "Attack 2", NormalisableRange<float> (0.01f, 5.0f), 0.01),
                        std::make_unique<AudioParameterFloat>("DECAY_ID_1", "Decay 2", NormalisableRange<float> (0.01f, 2.0f), 0.01),
                        std::make_unique<AudioParameterFloat>("SUSTAIN_ID_1", "Sustain 2", NormalisableRange<float> (0.0f, 1.0f), 1),
                        std::make_unique<AudioParameterFloat>("RELEASE_ID_1", "Release 2", NormalisableRange<float> (0.01f, 5.0f), 0),
                        std::make_unique<AudioParameterBool> ("BUTTON_L_ID_1", "Left button 2", false),
                        std::make_unique<AudioParameterBool> ("BUTTON_R_ID_1", "Right button 2", false),
                        std::make_unique<AudioParameterFloat>("FILTER_TYPE_ID_1", "Filter type 2", NormalisableRange<float>(0.0f, 2.0f), 0.0f),
                        std::make_unique<AudioParameterFloat>("CUTOFF_ID_1", "Cutoff 2", NormalisableRange<float>(20.0f, 16000.0f), 8000.0f),
                        std::make_unique<AudioParameterFloat>("RESONANCE_ID_1", "Resonance 2", NormalisableRange<float>(1.0f, 5.0f), 1.0f),
                        std::make_unique<AudioParameterFloat>("LEFT_BOUND_ID_1", "Start sample 2", NormalisableRange<float>(0.0f, 1080.0f), 0.0f),
                        std::make_unique<AudioParameterFloat>("RIGHT_BOUND_ID_1", "End sample 2", NormalisableRange<float>(0.0f, 1080.0f), 1080.0f),
                        std::make_unique<AudioParameterFloat>("GAIN_ID_1", "Gain Osc 2", NormalisableRange<float>(0.0f, 1.0f), 0.6f),
                        std::make_unique<AudioParameterFloat>("TUNE_ID_1", "Tune Osc 2", NormalisableRange<float>(-2.0f, 2.0f), 0.0f),
                        std::make_unique<AudioParameterBool> ("PHASE_1", "Invert phase 2", false),
                     })
#endif
{
  parameters.addParameterListener("MODE_ID", this);
  parameters.addParameterListener("POINT_ID", this);
  parameters.addParameterListener("FACTOR_ID", this);
  parameters.addParameterListener("COURSE_ID", this);
  parameters.addParameterListener("MEASURE_ID", this);

  parameters.addParameterListener("BUTTON_L_ID", this);
  parameters.addParameterListener("BUTTON_R_ID", this);

  parameters.addParameterListener("MODE_ID_1", this);
  parameters.addParameterListener("POINT_ID_1", this);
  parameters.addParameterListener("FACTOR_ID_1", this);
  parameters.addParameterListener("COURSE_ID_1", this);
  parameters.addParameterListener("MEASURE_ID_1", this);

  parameters.addParameterListener("BUTTON_L_ID_1", this);
  parameters.addParameterListener("BUTTON_R_ID_1", this);

  parameters.addParameterListener("PHASE", this);
  parameters.addParameterListener("PHASE_1", this);

}

GeoHeadAudioProcessor::~GeoHeadAudioProcessor()
{
}

//==============================================================================
const juce::String GeoHeadAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool GeoHeadAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool GeoHeadAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool GeoHeadAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double GeoHeadAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int GeoHeadAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int GeoHeadAudioProcessor::getCurrentProgram()
{
    return 0;
}

void GeoHeadAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String GeoHeadAudioProcessor::getProgramName (int index)
{
    return {};
}

void GeoHeadAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void GeoHeadAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
  ignoreUnused(samplesPerBlock);
  lastSampleRate = sampleRate;

  createWaveformTemplates();
  setParameters();

  if (*modeParameter == 1.0f || *modeParameter ==  2.0f || *modeParameter == 3.0f || *modeParameter == 4.0f)
  {
    createWavetableCardioid(modeParameter, pointParameter, factorParameter, courseParameter, measureParameter, 0);
    createWavetableCardioid(modeParameter_1, pointParameter_1, factorParameter_1, courseParameter_1, measureParameter_1, 1);
  } else
  {
        //Create wavetable draw
  }

  proc0.setWavetable(&waveforms);
  proc1.setWavetable(&waveforms);

  proc0.setParameters(parameters.getRawParameterValue("ATTACK_ID"),
                      parameters.getRawParameterValue("DECAY_ID"),
                      parameters.getRawParameterValue("SUSTAIN_ID"),
                      parameters.getRawParameterValue("RELEASE_ID"),
                      parameters.getRawParameterValue("GAIN_ID"),
                      parameters.getRawParameterValue("TUNE_ID"),
                      parameters.getRawParameterValue("LEFT_BOUND_ID"),
                      parameters.getRawParameterValue("RIGHT_BOUND_ID"),
                      parameters.getRawParameterValue("FILTER_TYPE_ID"),
                      parameters.getRawParameterValue("CUTOFF_ID"),
                      parameters.getRawParameterValue("RESONANCE_ID")
                    );

  proc1.setParameters(parameters.getRawParameterValue("ATTACK_ID_1"),
                      parameters.getRawParameterValue("DECAY_ID_1"),
                      parameters.getRawParameterValue("SUSTAIN_ID_1"),
                      parameters.getRawParameterValue("RELEASE_ID_1"),
                      parameters.getRawParameterValue("GAIN_ID_1"),
                      parameters.getRawParameterValue("TUNE_ID_1"),
                      parameters.getRawParameterValue("LEFT_BOUND_ID_1"),
                      parameters.getRawParameterValue("RIGHT_BOUND_ID_1"),
                      parameters.getRawParameterValue("FILTER_TYPE_ID_1"),
                      parameters.getRawParameterValue("CUTOFF_ID_1"),
                      parameters.getRawParameterValue("RESONANCE_ID_1")
                  );

  proc0.prepareToPlay(sampleRate, lastSampleRate);
  proc1.prepareToPlay(sampleRate, lastSampleRate);
}

void GeoHeadAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool GeoHeadAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void GeoHeadAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    //auto totalNumInputChannels  = getTotalNumInputChannels();
    //auto totalNumOutputChannels = getTotalNumOutputChannels();
    buffer.clear();
    proc0.processBlock(buffer, midiMessages);
    proc1.processBlock(buffer, midiMessages);

}

//==============================================================================
bool GeoHeadAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* GeoHeadAudioProcessor::createEditor()
{
    return new GeoHeadAudioProcessorEditor (*this);
}

//==============================================================================
void GeoHeadAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
  auto state = parameters.copyState();
  std::unique_ptr<juce::XmlElement> xml (state.createXml());
  copyXmlToBinary (*xml, destData);
}

void GeoHeadAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
  std::unique_ptr<juce::XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));

  if (xmlState.get() != nullptr)
      if (xmlState->hasTagName (parameters.state.getType()))
          parameters.replaceState (juce::ValueTree::fromXml (*xmlState));
}

void GeoHeadAudioProcessor::parameterChanged(const String & parameterID, float newValue)
{
  setParameters();

  if(parameterID == "PHASE")
  {
    if(newValue == true)
    {
      phase[0] = -1.0f;
    }
    else
    {
      phase[0] = 1.0f;
    }
  }

  if(parameterID == "PHASE_1")
  {
    if(newValue == true)
    {
      phase[1] = -1.0f;
    }
    else
    {
      phase[1] = 1.0f;
    }
  }

  if(parameterID == "BUTTON_L_ID")
  {
    if(algorithm[0] < 2)
    {
      algorithm[0] = 3;
    } else
    {
      algorithm[0]--;
    }
  }

  if(parameterID == "BUTTON_R_ID")
  {
    if(algorithm[0] > 2)
    {
      algorithm[0] = 1;
    } else
    {
      algorithm[0]++;
    }
  }

  if(parameterID == "BUTTON_L_ID_1")
  {
    if(algorithm[1] < 2)
    {
      algorithm[1] = 3;
    } else
    {
      algorithm[1]--;
    }
  }

  if(parameterID == "BUTTON_R_ID_1")
  {
    if(algorithm[1] > 2)
    {
      algorithm[1] = 1;
    } else
    {
      algorithm[1]++;
    }
  }
  createWavetableCardioid(modeParameter, pointParameter, factorParameter, courseParameter, measureParameter, 0);
  createWavetableCardioid(modeParameter_1, pointParameter_1, factorParameter_1, courseParameter_1, measureParameter_1, 1);
}

void GeoHeadAudioProcessor::setParameters()
{
  modeParameter = parameters.getRawParameterValue ("MODE_ID");
  pointParameter  = parameters.getRawParameterValue ("POINT_ID");
  factorParameter  = parameters.getRawParameterValue ("FACTOR_ID");
  courseParameter  = parameters.getRawParameterValue ("COURSE_ID");
  measureParameter = parameters.getRawParameterValue("MEASURE_ID");

  modeParameter_1 = parameters.getRawParameterValue ("MODE_ID_1");
  pointParameter_1  = parameters.getRawParameterValue ("POINT_ID_1");
  factorParameter_1  = parameters.getRawParameterValue ("FACTOR_ID_1");
  courseParameter_1  = parameters.getRawParameterValue ("COURSE_ID_1");
  measureParameter_1 = parameters.getRawParameterValue("MEASURE_ID_1");
}

void GeoHeadAudioProcessor::createWaveformTemplates()
{

  for(int i = 0; i < 1080; i++)
  {
    waveform_templates[0][i] = sin( MathConstants<float>::twoPi * i / 1080);
  }

  float mid;
  for(int i = 0; i < 1080; i++)
  {
    mid = i / 1080.0f;
    if( mid <= 0.5f)
    {
      waveform_templates[1][i] = 1.0f;
    } else
    {
      waveform_templates[1][i] = -1.0f;
    }
  }

    for(int i = 0; i < 1080; i++)
    {
      waveform_templates[2][i] = (i / 1080.0f) * 2 - 1.0;
    }

  float value;
  for(int i = 0; i < 1080; i++)
  {
      value = i / 1080.0f;
      if (value < 0.25)
      {
        waveform_templates[3][i] = value * 4;
      }
      else if (value < 0.75)
      {
        waveform_templates[3][i] = 2.0 - (value * 4.0);
      }
      else
      {
        waveform_templates[3][i] = value * 4 - 4.0;
      }
  }
}

void GeoHeadAudioProcessor::createWavetableCardioid(std::atomic<float>* mode, std::atomic<float>* points,
              std::atomic<float>* factor, std::atomic<float>* course, std::atomic<float>* measure, int id )
{
  /* It calculates the distance from the origin of the circle to where the line
   is drawn. At x points on every line. Then it calculates the average of the
   points while it's putting them in the wavetable array at the correct location
   maximum depth is 1/3 of a degree  */
  chord_lenght = 2 * sin( (MathConstants<float>::twoPi / *points / 2));
  int waveform_selection = static_cast<int>(*mode);
  waveform_selection--;


  int points_int = static_cast<int> (*points);
  for (int i=0; i < points_int; i++)
  {
    t = MathConstants<float>::twoPi * (i / *points);
    t_2 =  t * (*factor + *course);

    radian_x = cos(t);
    radian_y = sin(t);
    radian_x_2 = cos(t_2);
    radian_y_2 = sin(t_2);

    float increment = static_cast<int> (*measure);
    increment = 1 / increment;
    for(float j = 0.0f; j <= 1; j += increment)
    {
      x = j * radian_x + (1 - j) * radian_x_2;
      y = j * radian_y + (1 - j) * radian_y_2;
      rad_wav = std::sqrt(x * x + y * y);
      pos_wav = t + chord_lenght * j;

      while(pos_wav > MathConstants<float>::twoPi)
      {
         pos_wav -= MathConstants<float>::twoPi;
      }

      vec_pos = static_cast<int>( round ( pos_wav * (540 / (MathConstants<float>::pi))));

      rad_wav = rad_wav * waveform_templates[waveform_selection][vec_pos];

      if(waveform_holders[id][vec_pos] != 0.0f)
      {
        float total = (waveform_holders[id][vec_pos] + rad_wav) / 2;
        waveform_holders[id][vec_pos] = total;
      }
      else
      {
        waveform_holders[id][vec_pos] = rad_wav;
      }
    }
  }

  switch(algorithm[id])
  {
    case 1:
      {
        for(int i = 0; i < 1080; i++)
        {
          if(waveform_holders[id][i] != 0)
          {
            waveforms[id][i] = waveform_holders[id][i] * phase[id];
          } else
          {
            waveforms[id][i] = waveform_templates[waveform_selection][i] * phase[id];
          }
        }
        break;
      }
      case 2:
        {
          for(int i = 0; i < 1080; i++)
          {
            waveforms[id][i] = waveform_holders[id][i] * phase[id];
          }
          break;
        }
      case 3:
        {
          for(int i = 0; i < 1080; i++)
          {
            if(waveform_holders[id][i] != 0)
            {
              waveforms[id][i] = waveform_holders[id][i] * phase[id];
            } else
            {
              if(i == 0){
                waveforms[id][i] = 0;
              } else
              {
                waveforms[id][i] = waveforms[id][i - 1];
              }
            }
          }
          break;
        }
  }
  std::fill_n(waveform_holders[id], 1080, 0);
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new GeoHeadAudioProcessor();
}
