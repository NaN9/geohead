/*
  ==============================================================================

    ComponentFilter.cpp
    Created: 24 Jul 2020 10:15:55am
    Author:  eye

  ==============================================================================
*/

#include <JuceHeader.h>
#include "ComponentFilter.h"

//==============================================================================
ComponentFilter::ComponentFilter(GeoHeadAudioProcessor& p, int componentId):
    audioProcessor(p), id(componentId)
{
  setSize(200, 200);

  filterMenu.addItem("Low Pass", 1);
  filterMenu.addItem("High Pass", 2);
  filterMenu.addItem("Band Pass", 3);
  filterMenu.setJustificationType(Justification::centred);
  filterMenu.setColour(ComboBox::backgroundColourId, Colour(5, 7, 8));
  addAndMakeVisible(filterMenu);

  filterTypeAttachment = std::make_unique<AudioProcessorValueTreeState::ComboBoxAttachment>(audioProcessor.parameters, parameter_ids[id][0], filterMenu);

  filterCutoff.setSliderStyle(Slider::SliderStyle::Rotary);
  filterCutoff.setRange(20.0, 16000.0);
  filterCutoff.setValue (8000.0);
  filterCutoff.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
  filterCutoff.setSkewFactorFromMidPoint(1000.0);
  filterCutoff.setLookAndFeel(&knobC);
  filterCutoff.setSkewFactorFromMidPoint (1000);
  addAndMakeVisible(&filterCutoff);

  filterCutoffAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, parameter_ids[id][1], filterCutoff);

  filterRes.setSliderStyle(Slider::SliderStyle::Rotary);
  filterRes.setRange(1, 5);
  filterRes.setValue(1);
  filterRes.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
  filterRes.setLookAndFeel(&knobR);
  addAndMakeVisible(filterRes);

  filterResonanceAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, parameter_ids[id][2], filterRes);

}

ComponentFilter::~ComponentFilter()
{
}

void ComponentFilter::paint (juce::Graphics& g)
{

    juce::Rectangle<int> titleArea (7, 6, getWidth(), 20);

    g.setColour(Colours::white);
    g.drawText(title_ids[id], titleArea, Justification::centredTop);

}

void ComponentFilter::resized()
{
  filterMenu.setBounds(73, 110, 86, 20);
  filterCutoff.setBounds (30, 18, 86, 86);
  filterRes.setBounds (116, 18, 86, 86);

}
