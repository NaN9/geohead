/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
GeoHeadAudioProcessorEditor::GeoHeadAudioProcessorEditor (GeoHeadAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (1334, 825);
    addAndMakeVisible(cardioidComponent);
    addAndMakeVisible(cardioidComponent_1);
    addAndMakeVisible(wavetableComponent);
    addAndMakeVisible(wavetableComponent_1);
    addAndMakeVisible(filterComponent);
    addAndMakeVisible(filterComponent_1);

    //==========================================================================
    gainSlider.setSliderStyle (Slider::SliderStyle::Rotary);
    gainSlider.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
    gainSlider.setRange (0.0f, 1.0f);
    gainSlider.setValue (0.0f);
    gainSlider.setLookAndFeel(&knobRed);
    gainAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, "GAIN_ID", gainSlider);


    gainSlider_1.setSliderStyle (Slider::SliderStyle::Rotary);
    gainSlider_1.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
    gainSlider_1.setRange (0.0f, 1.0f);
    gainSlider_1.setValue (0.0f);
    gainSlider_1.setLookAndFeel(&knobRed);
    gainAttachment_1 = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, "GAIN_ID_1", gainSlider_1);

    tuneSlider.setSliderStyle (Slider::SliderStyle::Rotary);
    tuneSlider.setLookAndFeel(&knobOrange);
    tuneSlider.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
    tuneSlider.setRange (-4.0f, 4.0f);
    tuneSlider.setValue (0.0f);
    tuneAttachment = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, "TUNE_ID", tuneSlider);

    tuneSlider_1.setSliderStyle (Slider::SliderStyle::Rotary);
    tuneSlider_1.setTextBoxStyle (Slider::NoTextBox, false, 0, 0);
    tuneSlider_1.setLookAndFeel(&knobOrange);
    tuneSlider_1.setRange (-2.0f, 2.0f);
    tuneSlider_1.setValue (0.0f);
    tuneAttachment_1 = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(audioProcessor.parameters, "TUNE_ID_1", tuneSlider_1);

    addAndMakeVisible(gainSlider);
    addAndMakeVisible(gainSlider_1);
    addAndMakeVisible(tuneSlider);
    addAndMakeVisible(tuneSlider_1);

}

GeoHeadAudioProcessorEditor::~GeoHeadAudioProcessorEditor()
{
}

//==============================================================================
void GeoHeadAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    Image background = ImageCache::getFromMemory (BinaryData::background_png, BinaryData::background_pngSize);
    g.drawImageAt (background, 0, 0);
    g.drawImageAt (logo, 572, 37);

    g.setColour (juce::Colours::white);
    g.setFont (15.0f);
}

void GeoHeadAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    cardioidComponent.setBounds(0 , 0, 534, 510);
    cardioidComponent_1.setBounds(780, 0, 534, 510);

    wavetableComponent.setBounds(0, 510, 550, 315);
    wavetableComponent_1.setBounds(772, 510, 550, 315);

    filterComponent.setBounds(552, 240, 220, 200);
    filterComponent_1.setBounds(552, 390, 220, 200);

    gainSlider.setBounds(571, 650, 95, 95);
    gainSlider_1.setBounds(671, 650, 95, 95);
    tuneSlider.setBounds(571, 111, 95, 95);
    tuneSlider_1.setBounds(671, 111, 95, 95);

}
