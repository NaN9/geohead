# GeoHead

GeoHead is a stand-alone synthesiser & a VST-plugin that uses geometric patterns to generate wavetables.
These wavetables can be played back using midi notes.

It generates the wavetables by drawing lines on a circle and measuring the distance from the origin
to x amount of points on the line. It will multiply the measured values, with the values of the selected
waveform at the correct location on the circle. There are 1080 samples in one table. One sample per 1/3 of
a degree. A circle without any lines creates the original shape (Sine, saw, square or triangle).

It has a filter, an adsr, a pitch knob and an invert phase button. You can also select the range of the wavetable
ie where it starts and ends. Panning of individual oscillators will be implemented in a future release. Together
with new geometric shapes. You can change the mode with the arrow knobs, this will decide what to do with samples
that are not measured (They are empty in the table because there are more samples then measurement). This will create
some interesting distortions of the wavetable, but only if you turn down the measure knob. When you have the measure
knob open it will appear as if it is not doing anything because the whole wavetable is filled with values.

![alt text1][logo]

[logo]: geohead-small.png "Gui"

## Description of knobs

#### Around the circle
  1. Upper left:   Points parameter (Amount of points on the circle)
  2. Upper right:  Multiplier parameter
  3. Bottom Left:  Multiplier parameter fine
  4. Bottom right: Measure parameter (amount of measurements per line)

#### Around waveform
  1. Combobox:    Select waveform and geometric shape
  2. Arrows:      Select sample entry algorithm
  3. ADSR:        Attack sustain decay release
  4. phase-btn:   Flip phase
  5. Right knobs: Sample Range

#### Center of screen
  1. T-knobs: Tuning
  2. C-knobs: Cutoff
  3. R-knobs: Resonance
  4. G-knobs: Gain of individual oscillators

  Filter 1 is for the left oscillator, filter2 for the right one.
  All buttons on left side are for left oscillator, right side for the right one.      

## Building

So far I've only built under Linux, and tested in Bitwig, soon I'll release binaries for windows and mac OS.

Windows Build Instructions

1.  Download Juce (http://www.juce.com/)
2.  Run "The Projucer" executable included in Juce.
3.  Open GeoHead.jucer
4.  Click on Exporters, click on + and add Visual Studio
5.  Hit "Save Project and Open in Visual Studio".
6.  Select the build: "Release - 64-bit" and set platform to x64.
    Build!

Linux Build Instructions

1.  Download and Install Juce (http://www.juce.com)
2.  cd to Builds/LinuxMakefile/
3.  Run 'Make-clean' then  'make'


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
GPLv2

#### Special thanks to Yoshi Ono for creating the Graphics
