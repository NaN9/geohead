/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#pragma once

namespace BinaryData
{
    extern const char*   phase_down_png;
    const int            phase_down_pngSize = 616;

    extern const char*   phase_up_png;
    const int            phase_up_pngSize = 630;

    extern const char*   logo_png;
    const int            logo_pngSize = 4678;

    extern const char*   arrow_L_png;
    const int            arrow_L_pngSize = 1114;

    extern const char*   arrow_png;
    const int            arrow_pngSize = 626;

    extern const char*   knob2_png;
    const int            knob2_pngSize = 5791;

    extern const char*   DINPro_Light_ttf;
    const int            DINPro_Light_ttfSize = 88240;

    extern const char*   background_circle_png;
    const int            background_circle_pngSize = 7892;

    extern const char*   background_png;
    const int            background_pngSize = 30563;

    extern const char*   knob1_png;
    const int            knob1_pngSize = 3587;

    // Number of elements in the namedResourceList and originalFileNames arrays.
    const int namedResourceListSize = 10;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Points to the start of a list of resource filenames.
    extern const char* originalFilenames[];

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes);

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding original, non-mangled filename (or a null pointer if the name isn't found).
    const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8);
}
