# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/eye/Develop/GeoHead/GeoHead/JuceLibraryCode/include_juce_audio_plugin_client_Standalone.cpp" "/home/eye/Develop/GeoHead/GeoHead/Builds/CLion/cmake-build-debug/CMakeFiles/STANDALONE_PLUGIN.dir/home/eye/Develop/GeoHead/GeoHead/JuceLibraryCode/include_juce_audio_plugin_client_Standalone.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DEBUG=1"
  "JUCER_LINUX_MAKE_6D53C8B4=1"
  "JUCE_APP_VERSION=0.0.1"
  "JUCE_APP_VERSION_HEX=0x1"
  "JUCE_DISPLAY_SPLASH_SCREEN=0"
  "JUCE_GLOBAL_MODULE_SETTINGS_INCLUDED=1"
  "JUCE_MODULE_AVAILABLE_juce_audio_basics=1"
  "JUCE_MODULE_AVAILABLE_juce_audio_devices=1"
  "JUCE_MODULE_AVAILABLE_juce_audio_formats=1"
  "JUCE_MODULE_AVAILABLE_juce_audio_plugin_client=1"
  "JUCE_MODULE_AVAILABLE_juce_audio_processors=1"
  "JUCE_MODULE_AVAILABLE_juce_audio_utils=1"
  "JUCE_MODULE_AVAILABLE_juce_core=1"
  "JUCE_MODULE_AVAILABLE_juce_data_structures=1"
  "JUCE_MODULE_AVAILABLE_juce_dsp=1"
  "JUCE_MODULE_AVAILABLE_juce_events=1"
  "JUCE_MODULE_AVAILABLE_juce_graphics=1"
  "JUCE_MODULE_AVAILABLE_juce_gui_basics=1"
  "JUCE_MODULE_AVAILABLE_juce_gui_extra=1"
  "JUCE_PROJUCER_VERSION=0x60001"
  "JUCE_STANDALONE_APPLICATION=JucePlugin_Build_Standalone"
  "JUCE_STRICT_REFCOUNTEDPOINTER=1"
  "JUCE_USE_DARK_SPLASH_SCREEN=1"
  "JUCE_VST3_CAN_REPLACE_VST2=0"
  "JucePlugin_AAXCategory=2048"
  "JucePlugin_AAXDisableBypass=0"
  "JucePlugin_AAXDisableMultiMono=0"
  "JucePlugin_AAXIdentifier=com.RatKidPlugins.GeoHead"
  "JucePlugin_AAXManufacturerCode=JucePlugin_ManufacturerCode"
  "JucePlugin_AAXProductId=JucePlugin_PluginCode"
  "JucePlugin_AUExportPrefix=GeoHeadAU"
  "JucePlugin_AUExportPrefixQuoted=\"GeoHeadAU\""
  "JucePlugin_AUMainType='aumu'"
  "JucePlugin_AUManufacturerCode=JucePlugin_ManufacturerCode"
  "JucePlugin_AUSubType=JucePlugin_PluginCode"
  "JucePlugin_Build_AAX=0"
  "JucePlugin_Build_AU=0"
  "JucePlugin_Build_AUv3=0"
  "JucePlugin_Build_RTAS=0"
  "JucePlugin_Build_Standalone=1"
  "JucePlugin_Build_Unity=0"
  "JucePlugin_Build_VST3=1"
  "JucePlugin_Build_VST=0"
  "JucePlugin_CFBundleIdentifier=com.RatKidPlugins.GeoHead"
  "JucePlugin_Desc=\"GeoHead\""
  "JucePlugin_EditorRequiresKeyboardFocus=0"
  "JucePlugin_Enable_IAA=0"
  "JucePlugin_IAAName=\"RatKid Plugins: GeoHead\""
  "JucePlugin_IAASubType=JucePlugin_PluginCode"
  "JucePlugin_IAAType=0x61757269"
  "JucePlugin_IsMidiEffect=0"
  "JucePlugin_IsSynth=1"
  "JucePlugin_Manufacturer=\"RatKid Plugins\""
  "JucePlugin_ManufacturerCode=0x4d616e75"
  "JucePlugin_ManufacturerEmail=\"\""
  "JucePlugin_ManufacturerWebsite=\"\""
  "JucePlugin_Name=\"GeoHead\""
  "JucePlugin_PluginCode=0x566f6f70"
  "JucePlugin_ProducesMidiOutput=0"
  "JucePlugin_RTASCategory=2048"
  "JucePlugin_RTASDisableBypass=0"
  "JucePlugin_RTASDisableMultiMono=0"
  "JucePlugin_RTASManufacturerCode=JucePlugin_ManufacturerCode"
  "JucePlugin_RTASProductId=JucePlugin_PluginCode"
  "JucePlugin_VSTCategory=kPlugCategSynth"
  "JucePlugin_VSTNumMidiInputs=1"
  "JucePlugin_VSTNumMidiOutputs=16"
  "JucePlugin_VSTUniqueID=JucePlugin_PluginCode"
  "JucePlugin_Version=0.0.1"
  "JucePlugin_VersionCode=0x1"
  "JucePlugin_VersionString=\"0.0.1\""
  "JucePlugin_Vst3Category=\"Instrument|Synth\""
  "JucePlugin_WantsMidiInput=1"
  "LINUX=1"
  "_DEBUG=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/eye/Develop/JUCE/modules/juce_audio_processors/format_types/VST3_SDK"
  "../../../JuceLibraryCode"
  "/home/eye/Develop/JUCE/modules"
  "/usr/include/freetype2"
  "/usr/include/libpng16"
  "/usr/include/harfbuzz"
  "/usr/include/glib-2.0"
  "/usr/lib/glib-2.0/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/eye/Develop/GeoHead/GeoHead/Builds/CLion/cmake-build-debug/CMakeFiles/SHARED_CODE.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
